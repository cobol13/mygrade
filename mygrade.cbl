       IDENTIFICATION DIVISION. 
       PROGRAM-ID. mygrade.
       AUTHOR. YANISA.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT GRADE-FILE ASSIGN TO "mygrade.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVGGRADE-FILE ASSIGN TO "avggrade.txt"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION.
       FD  GRADE-FILE.
       01  GRADE-DETAIL.
           88 END-OF-GRADE-FILE VALUE  HIGH-VALUE.
           05 SUB-ID      PIC X(6).
           05 SUB-NAME    PIC X(50).
           05 SUB-CREDIT  PIC 9(1).
           05 SUB-GRADE   PIC X(2).
       FD  AVGGRADE-FILE.
       01  AVGGRADE-DETAIL.
           05 VAR-NAME      PIC X(20).
           05 AVG-GRADE     PIC 9(1)V9(3).

       WORKING-STORAGE SECTION.
       01  SUBSCI-ID            PIC X(1).
       01  SUBCS-ID             PIC X(2).
       01  NEW-GRADE            PIC 9(1)V9(2).   
       01  SUM-GRADE            PIC 9(3)V9(3).
       01  SUM-CREDIT           PIC 9(3).
       01  SUMAVG-GRADE         PIC 9(1)V9(3).


      *AVG-SCI-GRADE 
       01  CREDIT-SCI         PIC 9(3).
       01  SUMAVG-GRADESCI    PIC 9(1)V9(3).
       01  SUMCREDIT-GRADESCI PIC 9(3)V9(3).
      *AVG-CS-GRADE 
       01  CREDIT-CS      PIC 9(3).
       01  SUMAVG-GRADECS    PIC 9(1)V9(3).
       01  SUMCREDIT-GRADECS PIC 9(3)V9(3).


       PROCEDURE DIVISION.
       000-BEGIN. 
           OPEN INPUT  GRADE-FILE 
           OPEN OUTPUT AVGGRADE-FILE 
           PERFORM UNTIL END-OF-GRADE-FILE 
              READ GRADE-FILE 
                 AT END SET END-OF-GRADE-FILE TO TRUE 
              END-READ
              IF NOT END-OF-GRADE-FILE THEN
                 PERFORM 001-PROCESS THRU 001-EXIT 
              END-IF 

           END-PERFORM
           DISPLAY "AVG-GRADE  = "  SUMAVG-GRADE .
           DISPLAY "-----------------------------"
           DISPLAY "AVG-SCI-GRADE  = "  SUMAVG-GRADESCI
           DISPLAY "-----------------------------" 
           DISPLAY "AVG-CS-GRADE = "  SUMAVG-GRADECS
           DISPLAY "-----------------------------" 
           
                 MOVE "AVG-GRADE     : " TO VAR-NAME
           MOVE SUMAVG-GRADE   TO AVG-GRADE IN AVGGRADE-DETAIL
           WRITE AVGGRADE-DETAIL

           MOVE "AVG-SCI-GRADE : " TO VAR-NAME
           MOVE SUMAVG-GRADESCI  TO AVG-GRADE IN AVGGRADE-DETAIL
           WRITE AVGGRADE-DETAIL

           MOVE "AVG-CS-GRADE  : " TO VAR-NAME
           MOVE SUMAVG-GRADECS  TO AVG-GRADE IN AVGGRADE-DETAIL
           WRITE AVGGRADE-DETAIL

           CLOSE GRADE-FILE 
           CLOSE AVGGRADE-FILE
            
           GOBACK 
           .
       001-PROCESS.
            EVALUATE TRUE 
              WHEN SUB-GRADE ="A"  MOVE 4   TO NEW-GRADE   
              WHEN SUB-GRADE ="B+" MOVE 3.5 TO NEW-GRADE 
              WHEN SUB-GRADE ="B"  MOVE 3   TO NEW-GRADE  
              WHEN SUB-GRADE ="C+" MOVE 2.5 TO NEW-GRADE 
              WHEN SUB-GRADE ="C"  MOVE 2   TO NEW-GRADE 
              WHEN SUB-GRADE ="D+" MOVE 1.5 TO NEW-GRADE 
              WHEN SUB-GRADE ="D"  MOVE 1   TO NEW-GRADE 
              WHEN OTHER MOVE 0 TO NEW-GRADE 
           END-EVALUATE
           COMPUTE SUM-CREDIT = SUM-CREDIT +SUB-CREDIT 
           COMPUTE SUM-GRADE  = SUM-GRADE  + NEW-GRADE 
           * SUB-CREDIT.
           COMPUTE SUMAVG-GRADE =SUM-GRADE  / SUM-CREDIT.


           MOVE SUB-ID TO SUBSCI-ID
           IF SUBSCI-ID IS EQUAL TO "3" THEN
              COMPUTE CREDIT-SCI         = CREDIT-SCI + SUB-CREDIT 
              COMPUTE SUMCREDIT-GRADESCI = SUMCREDIT-GRADESCI 
              +(SUB-CREDIT * NEW-GRADE) 
              COMPUTE SUMAVG-GRADESCI    = SUMCREDIT-GRADESCI 
              / CREDIT-SCI  
           END-IF.

           MOVE SUB-ID TO SUBCS-ID
           IF SUBCS-ID IS EQUAL TO "31" THEN
              COMPUTE CREDIT-CS         = CREDIT-CS + SUB-CREDIT 
              COMPUTE SUMCREDIT-GRADECS = SUMCREDIT-GRADECS 
              +(SUB-CREDIT * NEW-GRADE) 
              COMPUTE SUMAVG-GRADECS    = SUMCREDIT-GRADECS / CREDIT-CS  
           END-IF.


       001-EXIT.
           EXIT.
           